# munin-gen8

Some plugins to monitor temperatures of each SMART Array controllers and attached disks.

At this time only tested on HP Microserver Gen8 with Smart Array P222.

## Requirement

This plugins need `ssacli` and `hpacucli` (from [hwraid](http://hwraid.le-vert.net/wiki/DebianPackages)) to work correctly.

## Installation

```bash
$ cd /opt
$ sudo git clone https://framagit.org/dbroqua/munin-gen8.git
$ cd /etc/munin/plugins
$ sudo -ln -snf /opt/munin-gen8/smart_array_ ./
$ sudo /etc/init.d/munin-node restart
```

## Examples

Only monitor disks temperature (smart_array_disks_temperature)

![smart_array_disks_temperature](examples/smart_array_disks_temperature-day.png "smart_array_disks_temperature")

Only monitor controllers temperature (smart_array_system_temperature)

![smart_array_system_temperature](examples/smart_array_system_temperature-day.png "smart_array_system_temperature")

Monitor controllers and disks (smart_array_temperatures)

![smart_array_temperatures](examples/smart_array_temperatures-day.png "smart_array_temperatures")
